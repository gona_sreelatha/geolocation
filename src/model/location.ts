export interface Location {
    locationTimeStamp: number;
    latitude: number;
    longitude: number;
    accuracy: number;
    speed: number;
    heading: number;
    misc: object;
  }
  