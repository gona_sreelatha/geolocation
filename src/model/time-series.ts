export interface TimeSeries {
  timeStamp: number;  
  locationTimeStamp: number;
  eventTimeStamp: number;
  eventCode: number;
  eventDetails: object;
  latitude: number;
  longitude: number;
  accuracy: number;
  speed: number;
  misc: object;
}
