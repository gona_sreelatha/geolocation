import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';

import { Observable } from 'rxjs';

declare var SMS: any;

@Injectable()
export class SmsTrackerProvider {
  constructor(
    public http: HttpClient,
    public platform: Platform
  ) { }

  readSMS() {
    return new Observable(observer => {
      this.platform.ready().then((readySource) => {
        if (SMS) SMS.startWatch(() => {
          console.log('SMS_WATCH - Registeration successful');
        }, err => {
          console.log('SMS_WATCH - Registeration failed.');
        });
        document.addEventListener('onSMSArrive', (e: any) => {
          observer.next(e.data);
        });
      });
    });
  }

  stopReadSMS() {
    if (SMS) SMS.stopWatch();
  }
}