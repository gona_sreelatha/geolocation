import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation'
import { Location } from '../../model/location';

@Injectable()
export class LocationTrackerProvider {
  public watch: any;
  private location: Location;
  public isForground = true;
  private userLocation = new BehaviorSubject<any>({});
  private interval;
  private frequency = 5000;
  private state = 'FG';

  constructor(
    public zone: NgZone,
    public geolocation: Geolocation,
    public backgroundGeolocation: BackgroundGeolocation,
  ) { }

  public getUserLocation() {
    return this.userLocation.asObservable();
  }

  public startTracking() {
    this.clearRegistraions();
    if (this.state == 'FG') {
      this.fgTracking();
    } else if (this.state == 'BG') {
      this.bgTracking();
    }
  }

  public changeAppState(state) {
    this.state = state;
    console.log('STATE_CHANGE to ' + state);
    this.clearRegistraions();
    if (state == 'FG') {
      this.fgTracking();
    } else if (state == 'BG') {
      this.bgTracking();
    }
  }

  public stopTracking() {
    this.clearRegistraions();
  }

  private registerSendLocation() {
    this.interval = setInterval(() => {
      console.log("REGISTER_SEND_LOCATION");
      if (this.location) {
        this.userLocation.next(this.location);
      }
    }, this.frequency)
  }

  private fgTracking() {
    console.log("FG_TRACKING");
    this.registerSendLocation();
    this.watch = this.geolocation.watchPosition({
      enableHighAccuracy: true
    }).subscribe((fgPosition: Geoposition) => {
      console.log("FG_LOCATION_SUCCESS");
      // Run update inside of Angular's zone
      this.zone.run(() => {
        this.location = {
          latitude: fgPosition.coords.latitude,
          longitude: fgPosition.coords.longitude,
          misc: {
            type: 'FG'
          },
          accuracy: fgPosition.coords.accuracy,
          speed: fgPosition.coords.speed,
          locationTimeStamp: fgPosition.timestamp,
          heading: fgPosition.coords.heading
        };
      });
    });
  }

  private bgTracking() {
    console.log("BG_TRACKING");
    this.backgroundGeolocation.configure({
      desiredAccuracy: 50,
      stationaryRadius: 10,
      distanceFilter: 10,
      locationProvider: 0,
      debug: false,
      interval: this.frequency
    }).subscribe((bgLocation: BackgroundGeolocationResponse) => {
      console.log("BG_LOCATION_SUCCESS" + JSON.stringify(bgLocation));
      // Run update inside of Angular's zone
      this.zone.run(() => {
        let location: Location = {
          latitude: bgLocation.latitude,
          longitude: bgLocation.longitude,
          misc: {
            type: 'BG'
          },
          accuracy: bgLocation.accuracy,
          speed: bgLocation.speed,
          locationTimeStamp: bgLocation.time,
          heading: bgLocation.bearing
        };
        this.userLocation.next(location);
        this.backgroundGeolocation.finish();
      });
    }, (err) => {
      console.log("BG_LOCATION_ERROR " + err);
    });

    // Turn ON the background-geolocation system.
    this.backgroundGeolocation.start();
  }

  private clearRegistraions() {
    this.location = null;
    this.backgroundGeolocation.stop();
    if (this.watch) {
      this.watch.unsubscribe();
    }
    if (this.interval) {
      clearInterval(this.interval);
    }
  }
}