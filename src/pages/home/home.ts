import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController, Platform } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';
import { Device } from '@ionic-native/device';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { BackgroundMode } from '@ionic-native/background-mode';
import { AppMinimize } from '@ionic-native/app-minimize';

import { LocationTrackerProvider } from '../../providers/location-tracker/location-tracker';
import { SmsTrackerProvider } from '../../providers/sms-tracker/sms-tracker';
import { EVENT_CODE } from '../../config/event-code';

import { TimeSeries } from '../../model/time-series';
import { Location } from '../../model/location';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage implements OnInit {
  private flags = {
    sms: false,
    location: false
  };

  private locationSubscription: Subscription;
  private smsSubscription: Subscription;

  private location: Location;
  public sms: any;

  private timeSeriesApiBasePath = `https://api.timeseries.nm30.ml/v1/api/`;

  private locationCount = 0;
  private smsCount = 0;

  constructor(
    protected platform: Platform,
    protected device: Device,
    protected httpClient: HttpClient,
    protected navCtrl: NavController,
    protected smsTracker: SmsTrackerProvider,
    protected androidPermissions: AndroidPermissions,
    private backgroundMode: BackgroundMode,
    private appMinimize: AppMinimize,
    protected googlePlus: GooglePlus,
    protected locationTracker: LocationTrackerProvider
  ) { }

  ngOnInit() {
    this.googlePlus.login({}).then(res => {
      console.log("LOGIN_GOOGLE_SUCCESFULL" + JSON.stringify(res));
      this.mapDeviceToUser(res.userId);
    }).catch(err => {
      console.log("LOGIN_GOOGLE_ERROR" + err);
    });

    this.platform.registerBackButtonAction(() => {
      this.appMinimize.minimize();
    });
    this.backgroundMode.disableWebViewOptimizations();
  }

  /**
   * Toogle between watch
   *
   * @param entity
   */
  public toggle(entity: string) {
    this.flags[entity] = !this.flags[entity];

    if (this.flags.sms || this.flags.location) {
      this.backgroundMode.enable();
      console.log("BG_ENABLED");
    } else {
      this.backgroundMode.disable();
      console.log("BG_DISABLED");
    }
    this.backgroundMode.on('activate').subscribe(() => {
      console.log("BG_ACTIVATED");
      this.locationTracker.changeAppState('BG');
    })

    this.backgroundMode.on('deactivate').subscribe(() => {
      console.log("BG_DEACTIVATED");
      this.locationTracker.changeAppState('FG');
    })

    if (this.flags[entity]) {
      console.log("START TRACKING %s", entity.toUpperCase());
      if (entity == 'sms') {
        this.startTrackingSms();
      } else if (entity == 'location') {
        this.startTrackingLocation();
      }
    } else {
      console.log("STOP TRACKING %s", entity.toUpperCase);
      if (entity == 'sms') {
        this.stopTrackingSms();
      } else if (entity == 'location') {
        this.stopTrackingLocation();
      }
    }
  }

  /**
   * Start tracking SMS
   */
  private startTrackingSms() {
    if (this.platform.is('cordova')) {
      console.log("SMS_START_TRACKING");
      this.checkAndRequestPermission(this.androidPermissions.PERMISSION.READ_SMS, this.watchSms);
    }
  }

  /**
   * Stop tracking SMS
   */
  private stopTrackingSms() {
    this.smsTracker.stopReadSMS();
    this.smsSubscription.unsubscribe();
    console.log("SMS_STOP_TRACKING");
  }

  /**
   * Register Watch SMS
   */
  private watchSms() {
    this.smsSubscription = this.smsTracker.readSMS().subscribe(sms => {
      this.sms = sms;
      this.smsCount++;
      this.updateEvent(EVENT_CODE.NEW_MESSAGE, this.sms.date_sent, {
        from: this.sms.address,
        body: this.sms.body
      }, {});
    }, err => {
      console.log("SMS_READ_ERROR, %s", err);
    });
  }

  /**
   * Start Tracking Location
   */
  private startTrackingLocation() {
    console.log("GPS_START_TRACKING_BEGIN");
    this.locationTracker.startTracking();
    this.locationSubscription = this.locationTracker.getUserLocation().subscribe((currentLocation: Location) => {
      console.log("GPS_LOCATION_UPDATE");
      if (currentLocation) {
        console.log("GPS_LOCATION " + JSON.stringify(currentLocation));
        this.locationCount++;
        this.location = currentLocation;
        this.updateEvent(EVENT_CODE.LOCATION_UPDATE, (new Date()).getTime(), {}, this.location.misc);
      } else {
        console.log("GPS_LOCATION_EMPTY");
      }
    });
  }

  /**
   * Stop Tracking Location
   */
  private stopTrackingLocation() {
    this.locationTracker.stopTracking();
    this.locationSubscription.unsubscribe();
    console.log("GPS_STOP_TRACKING");
  }

  /**
   * Method to check and request permission
   * for a given action
   *
   * @param action
   * @param callback
   */
  private checkAndRequestPermission(action: string, callback: Function) {
    // You're on a mobile device "IOS ANDROID WINDOWS"
    // now you can call your native plugins
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]).then(success => {
      callback.call(this);
    }, err => {
      console.log("PERMISSION not granted for %s", action);
    });
  }

  /**
   * Method to request permission
   * for a given action
   *
   * @param action
   * @param callback
   */
  private requestPermission(action, callback) {
    this.androidPermissions.requestPermission(action).then(success => {
      console.log('PERMISSION already granted for %s', action);
      callback.call(this);
    }, err => {
      console.log('PERMISSION not granted for %s', action);
    })
  }

  /**
   * Uploads the event data
   *
   * @param eventCode
   * @param eventDetails
   */
  private updateEvent(eventCode: number, eventTimeStamp: number, eventDetails: object, misc: object) {
    if (this.location && this.location.latitude && this.location.longitude) {
      let date = new Date();
      let timeSeries: TimeSeries = {
        timeStamp: date.getTime(),
        latitude: this.location.latitude,
        longitude: this.location.longitude,
        locationTimeStamp: this.location.locationTimeStamp,
        accuracy: this.location.accuracy,
        speed: this.location.speed ? this.location.speed : 0,
        misc: misc,
        eventCode: eventCode,
        eventDetails: eventDetails,
        eventTimeStamp: eventTimeStamp
      };
      this.httpClient.post(`${this.timeSeriesApiBasePath}time-series/${this.device.uuid}`, timeSeries).subscribe(response =>{
        console.log("TIME_SERIES_SUCCESSFUL");
      }, error => {
        console.log("TIME_SERIES_FAILED " + error);
      });
    }
  }

  /**
   * Maps the google user to the deviceId
   * @param deviceId
   */
  private mapDeviceToUser(googleUserId: string) {
    this.httpClient.post(`${this.timeSeriesApiBasePath}device/${this.device.uuid}`, {
      googleUserId: googleUserId
    }).subscribe(response =>{
      console.log("MAP_DEVICE_SUCCESSFUL");
    }, error => {
      console.log("MAP_DEVICE_FAILED " + error);
    });
  }
}